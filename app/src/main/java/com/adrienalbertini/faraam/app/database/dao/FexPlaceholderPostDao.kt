package com.adrienalbertini.faraam.app.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.adrienalbertini.faraam.app.database.FlowList
import com.adrienalbertini.faraam.app.database.entity.FexPlaceholderPostEntity
import com.adrienalbertini.faraam.fskeleton.database.FaraamBaseDao
import io.reactivex.Flowable


@Dao
interface FexPlaceholderPostDao: FaraamBaseDao<FexPlaceholderPostEntity> {
    @Query("SELECT * FROM placeholder_posts WHERE id = :postId")
    fun load(postId: Int): Flowable<FexPlaceholderPostEntity>

    @Query("SELECT * FROM placeholder_posts")
    fun getAll(): FlowList<FexPlaceholderPostEntity>
}