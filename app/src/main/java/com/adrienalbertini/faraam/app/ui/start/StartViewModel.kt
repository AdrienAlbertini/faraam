package com.adrienalbertini.faraam.app.ui.start

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adrienalbertini.faraam.app.database.repository.FexPlaceholderPostRepo
import javax.inject.Inject

class StartViewModel @Inject constructor(fexPostsRepository: FexPlaceholderPostRepo) : ViewModel() {
    private val _data = MutableLiveData<String>()
    val data: LiveData<String>
        get() = _data

    val postsData = LiveDataReactiveStreams.fromPublisher(fexPostsRepository.loadContent())
}