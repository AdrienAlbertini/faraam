package com.adrienalbertini.faraam.app.di.module

import com.adrienalbertini.faraam.app.ui.end.EndFragment
import com.adrienalbertini.faraam.app.ui.start.StartFragment
import com.adrienalbertini.faraam.fskeleton.di.FaraamFragmentBuildersModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FexFragmentBuildersModule: FaraamFragmentBuildersModule() {
    @ContributesAndroidInjector abstract fun contributeStartFragment(): StartFragment
    @ContributesAndroidInjector abstract fun contributeEndFragment(): EndFragment
}