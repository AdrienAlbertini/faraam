package com.adrienalbertini.faraam.app.app

import android.app.Application
import com.adrienalbertini.faraam.app.di.module.FexViewModelModule
import dagger.Binds
import dagger.Module



@Module(includes = [
    FexViewModelModule::class
])
abstract class FexAppModule {
    @Binds internal abstract fun application(app: FexApp): Application
}