package com.adrienalbertini.faraam.app.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(
        tableName = "placeholder_posts"
)
data class FexPlaceholderPostEntity (
        @PrimaryKey var id: Int,
        @SerializedName("title") var title: String,
        @SerializedName("body") var body: String
)