package com.adrienalbertini.faraam.app.di.module

import androidx.lifecycle.ViewModel
import com.adrienalbertini.faraam.app.ui.end.EndViewModel
import com.adrienalbertini.faraam.app.ui.start.StartViewModel
import com.adrienalbertini.faraam.fskeleton.di.FaraamViewModelKey
import com.adrienalbertini.faraam.fskeleton.di.FaraamViewModelModule
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class FexViewModelModule: FaraamViewModelModule() {
    @Binds
    @IntoMap
    @FaraamViewModelKey(EndViewModel::class)
    internal abstract fun endViewModel(viewModel: EndViewModel): ViewModel

    @Binds
    @IntoMap
    @FaraamViewModelKey(StartViewModel::class)
    internal abstract fun startViewModel(viewModel: StartViewModel): ViewModel
}