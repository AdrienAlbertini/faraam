package com.adrienalbertini.faraam.app.ui.start

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.adrienalbertini.faraam.app.R
import com.adrienalbertini.faraam.app.database.Resource
import com.adrienalbertini.faraam.app.ui.base.BaseFragment
import com.adrienalbertini.faraam.fskeleton.common.Log
import com.adrienalbertini.faraam.fskeleton.common.extensions.nonNull
import com.adrienalbertini.faraam.fskeleton.common.extensions.withViewModel
import com.adrienalbertini.faraam.fskeleton.di.FaraamViewModelFactory
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_start.*
import javax.inject.Inject

class StartFragment: BaseFragment() {

    companion object {
        fun newInstance() = StartFragment()

        const val KITTEN_URL = "http://goo.gl/gEgYUd"
    }

    @Inject lateinit var viewModelFactory: FaraamViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Glide example
        Glide.with(this)
                .load(R.drawable.ic_launcher_foreground)
                .into(this.image)

        withViewModel<StartViewModel> (viewModelFactory) {
            // Observe data on the ViewModel, exposed as a LiveData
            data.nonNull().observe(this@StartFragment, Observer { data ->
                // Set the text exposed by the LiveData
                this@StartFragment.text.text = data
            })

            postsData.nonNull().observe(this@StartFragment, Observer {
                when(it) {
                    is Resource.Success -> run {
                    }
                    is Resource.Failure -> run {
                    }
                }
            })
        }

        // Set up a click listener on the login button
        this.navigate_bt.setOnClickListener {
            // Navigate to the login destination
            Navigation.findNavController(it).navigate(R.id.end_action)
        }
    }

}