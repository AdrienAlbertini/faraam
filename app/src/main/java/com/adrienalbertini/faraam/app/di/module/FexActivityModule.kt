package com.adrienalbertini.faraam.app.di.module

import com.adrienalbertini.faraam.app.ui.start.StartActivity
import com.adrienalbertini.faraam.fskeleton.di.FaraamActivityModule
import com.adrienalbertini.faraam.fskeleton.di.UILifecycleScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FexActivityModule: FaraamActivityModule() {
    @UILifecycleScope @ContributesAndroidInjector(modules = [FexFragmentBuildersModule::class])
    abstract fun startActivity(): StartActivity
}
