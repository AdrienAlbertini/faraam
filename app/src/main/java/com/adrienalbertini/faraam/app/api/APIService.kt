package com.adrienalbertini.faraam.app.api

import com.adrienalbertini.faraam.app.database.entity.FexPlaceholderPostEntity
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

typealias ObsResponse<ResultType> = Observable<Response<ResultType>>
typealias ObsResponseList<ResultType> = Observable<Response<List<ResultType>>>

interface APIService {
    @GET("/posts")
    fun placeholderPosts(): ObsResponseList<FexPlaceholderPostEntity>
}