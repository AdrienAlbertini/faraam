package com.adrienalbertini.faraam.app.di.component

import com.adrienalbertini.faraam.app.ui.end.EndViewModel
import com.adrienalbertini.faraam.app.ui.start.StartViewModel
import dagger.Subcomponent


@Subcomponent
interface FexViewModelSubComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): FexViewModelSubComponent
    }

    fun startViewModel(): StartViewModel
    fun endViewModel(): EndViewModel
}