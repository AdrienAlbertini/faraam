package com.adrienalbertini.faraam.app.app

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.adrienalbertini.faraam.app.database.dao.FexPlaceholderPostDao
import com.adrienalbertini.faraam.app.database.entity.FexPlaceholderPostEntity
import com.adrienalbertini.faraam.fskeleton.common.typeconverters.CommonConverters
import com.adrienalbertini.faraam.fskeleton.common.typeconverters.DateConverter
import com.adrienalbertini.faraam.fskeleton.common.typeconverters.UriConverter

@Database(
        entities = [
            FexPlaceholderPostEntity::class
        ],
        version = FexDatabase.VERSION,
        exportSchema = false //TODO Set to true or remove when using database
)
@TypeConverters(
        CommonConverters::class,
        DateConverter::class,
        UriConverter::class
)
abstract class FexDatabase: RoomDatabase() {

    // DAO
    abstract fun fexPlaceholderPostDao(): FexPlaceholderPostDao

    companion object {
        const val VERSION = 1
        const val DB_NAME = "app_db"
    }
}