package com.adrienalbertini.faraam.app.app

import com.adrienalbertini.faraam.app.di.module.FexActivityModule
import com.adrienalbertini.faraam.app.di.module.FexDatabaseModule
import com.adrienalbertini.faraam.fskeleton.app.FaraamAppComponent
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton



@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    FexAppModule::class,
    FexDatabaseModule::class,
    FexActivityModule::class
])
interface FexAppComponent: FaraamAppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: FexApp): Builder

        fun build(): FexAppComponent
    }

    fun inject(app: FexApp)
}