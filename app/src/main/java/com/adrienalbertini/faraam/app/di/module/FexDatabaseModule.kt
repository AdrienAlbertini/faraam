package com.adrienalbertini.faraam.app.di.module

import android.app.Application
import androidx.room.Room
import com.adrienalbertini.faraam.app.BuildConfig
import com.adrienalbertini.faraam.app.api.APIService
import com.adrienalbertini.faraam.app.app.FexDatabase
import com.adrienalbertini.faraam.app.common.Constants
import com.adrienalbertini.faraam.app.database.dao.FexPlaceholderPostDao
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class FexDatabaseModule {

    @Singleton
    @Provides
    fun providesFexPlaceholderPostDao(db: FexDatabase): FexPlaceholderPostDao {
        return db.fexPlaceholderPostDao()
    }

    @Singleton
    @Provides
    fun providesDatabase(app: Application): FexDatabase {
        return if (BuildConfig.DEBUG) {
            Room.databaseBuilder(app, FexDatabase::class.java, FexDatabase.DB_NAME)
                    .build()
        } else {
            Room.databaseBuilder(app, FexDatabase::class.java, FexDatabase.DB_NAME)
                    .fallbackToDestructiveMigration()
                    .build()
        }
    }

    @Singleton
    @Provides
    fun provideAPIService(): APIService {
        return Retrofit.Builder()
                .baseUrl(Constants.API_BASE_PATH)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(OkHttpClient())
                .build()
                .create(APIService::class.java)
    }
}