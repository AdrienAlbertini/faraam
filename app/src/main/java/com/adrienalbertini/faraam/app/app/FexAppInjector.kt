package com.adrienalbertini.faraam.app.app

import com.adrienalbertini.faraam.fskeleton.app.FaraamAppInjector

object FexAppInjector: FaraamAppInjector<FexApp>() {

    override fun injectAppComponent(app: FexApp) {
        DaggerFexAppComponent.builder()
                .application(app)
                .build()
                .inject(app)
    }
}