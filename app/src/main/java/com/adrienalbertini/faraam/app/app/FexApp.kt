package com.adrienalbertini.faraam.app.app

import android.content.Context
import android.net.ConnectivityManager
import com.adrienalbertini.faraam.fskeleton.app.FaraamApp
import dagger.android.HasActivityInjector
import javax.inject.Inject

// Fex as Faraam Example
class FexApp @Inject constructor(): FaraamApp(), HasActivityInjector {
    companion object {

        lateinit var INSTANCE: FexApp

        fun appContext(): Context = INSTANCE.applicationContext

        fun isNetworkAvailable(): Boolean {
            val cm = INSTANCE.applicationContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            return cm?.activeNetworkInfo?.isConnected ?: false
        }
    }

    override fun injectApp(app: FaraamApp) {
        FexAppInjector.init(app as FexApp)
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        injectApp(this)
    }
}