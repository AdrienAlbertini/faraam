package com.adrienalbertini.faraam.app.database

import android.content.Context
import com.adrienalbertini.faraam.fskeleton.common.exceptions.FaraamNetworkEceptions
import com.adrienalbertini.faraam.fskeleton.common.extensions.isNetworkStatusAvailable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

typealias FlowCall<ResultType> = Flowable<Response<ResultType>>
typealias FlowCallList<ResultType> = Flowable<Response<List<ResultType>>>
typealias FlowRes<ResultType> = Flowable<Resource<ResultType>>
typealias FlowResList<ResultType> = Flowable<Resource<List<ResultType>>>
typealias FlowList<ResultType> = Flowable<List<ResultType>>
typealias NetList<ResultType, RequestType> = NetworkBoundResource<List<ResultType>, List<RequestType>>

abstract class NetworkBoundResource<ResultType, RequestType>(context: Context) {

    private val result: Flowable<Resource<ResultType>>

    init {
        // Lazy disk observable.
        val diskObservable = Flowable.defer {
            loadFromDb()
                    // Read from disk on Computation Scheduler
                    .subscribeOn(Schedulers.computation())
        }

        // Lazy network observable.
        val networkObservable = Flowable.defer {
            createCall()
                    // Request API on IO Scheduler
                    .subscribeOn(Schedulers.io())
                    // Read/Write to disk on Computation Scheduler
                    .observeOn(Schedulers.computation())
                    .doOnNext { request: Response<RequestType> ->
                        if (request.isSuccessful) {
                            saveCallResult(processResponse(request))
                        }
                    }
                    .onErrorReturn { throwable: Throwable ->
                        when (throwable) {
                            is HttpException -> {
                                throw FaraamNetworkEceptions.NoServerConnectivityError(context)
                            }
                            is IOException -> {
                                throw FaraamNetworkEceptions.NoNetworkConnectivityError(context)
                            }
                            else -> {
                                throw FaraamNetworkEceptions.UnexpectedError(context)
                            }
                        }
                    }
                    .flatMap { loadFromDb() }
        }

        result = when {
            context.isNetworkStatusAvailable() -> networkObservable
                    .map<Resource<ResultType>> { Resource.Success(it) }
                    .onErrorReturn { Resource.Failure(it) }
                    // Read results in Android Main Thread (UI)
                    .observeOn(AndroidSchedulers.mainThread())
                    .startWith(Resource.Loading())
            else -> diskObservable
                    .map<Resource<ResultType>> { Resource.Success(it) }
                    .onErrorReturn { Resource.Failure(it) }
                    // Read results in Android Main Thread (UI)
                    .observeOn(AndroidSchedulers.mainThread())
                    .startWith(Resource.Loading())
        }
    }

    fun asFlowable(): Flowable<Resource<ResultType>> {
        return result
    }

    private fun processResponse(response: Response<RequestType>): RequestType {
        return response.body()!!
    }

    protected abstract fun saveCallResult(result: RequestType)

    protected abstract fun loadFromDb(): Flowable<ResultType>

    protected abstract fun createCall(): Flowable<Response<RequestType>>
}