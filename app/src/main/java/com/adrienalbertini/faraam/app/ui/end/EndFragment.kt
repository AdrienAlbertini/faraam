package com.adrienalbertini.faraam.app.ui.end

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.adrienalbertini.faraam.app.R
import com.adrienalbertini.faraam.app.ui.base.BaseFragment
import com.adrienalbertini.faraam.app.ui.start.StartFragment
import com.adrienalbertini.faraam.fskeleton.common.extensions.nonNull
import com.adrienalbertini.faraam.fskeleton.common.extensions.withViewModel
import com.adrienalbertini.faraam.fskeleton.di.FaraamViewModelFactory
import kotlinx.android.synthetic.main.fragment_end.*
import javax.inject.Inject

class EndFragment : BaseFragment() {

    companion object {
        fun newInstance() = StartFragment()
    }

    @Inject lateinit var viewModelFactory: FaraamViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_end, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        withViewModel<EndViewModel>(viewModelFactory) {
            // Observe data on the ViewModel, exposed as a LiveData
            data.nonNull().observe(this@EndFragment, Observer { data ->
                // Set the text exposed by the LiveData
                this@EndFragment.end_text.text = data
            })
        }
    }
}