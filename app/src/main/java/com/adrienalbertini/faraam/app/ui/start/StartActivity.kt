package com.adrienalbertini.faraam.app.ui.start

import android.os.Bundle
import com.adrienalbertini.faraam.app.R
import com.adrienalbertini.faraam.app.ui.base.BaseActivity

class StartActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
    }
}
