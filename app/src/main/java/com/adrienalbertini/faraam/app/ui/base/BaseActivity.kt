package com.adrienalbertini.faraam.app.ui.base

import com.adrienalbertini.faraam.fskeleton.ui.FaraamBaseActivity
import dagger.android.support.HasSupportFragmentInjector

abstract class BaseActivity: FaraamBaseActivity(), HasSupportFragmentInjector