package com.adrienalbertini.faraam.app.database.repository

import com.adrienalbertini.faraam.app.api.APIService
import com.adrienalbertini.faraam.app.app.FexApp
import com.adrienalbertini.faraam.app.database.FlowCallList
import com.adrienalbertini.faraam.app.database.FlowList
import com.adrienalbertini.faraam.app.database.FlowResList
import com.adrienalbertini.faraam.app.database.NetList
import com.adrienalbertini.faraam.app.database.dao.FexPlaceholderPostDao
import com.adrienalbertini.faraam.app.database.entity.FexPlaceholderPostEntity
import io.reactivex.BackpressureStrategy
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FexPlaceholderPostRepo @Inject constructor(
        private val app: FexApp,
        private val dao: FexPlaceholderPostDao,
        private val apiService: APIService
) {

    fun loadContent(): FlowResList<FexPlaceholderPostEntity> {
        return object : NetList<FexPlaceholderPostEntity, FexPlaceholderPostEntity>(app.applicationContext) {
            override fun saveCallResult(result: List<FexPlaceholderPostEntity>) {
                dao.insertAll(result)
            }

            override fun loadFromDb(): FlowList<FexPlaceholderPostEntity> {
                return dao.getAll()
            }

            override fun createCall(): FlowCallList<FexPlaceholderPostEntity> {
                return apiService.placeholderPosts().toFlowable(BackpressureStrategy.LATEST)
            }
        }.asFlowable()
    }
}