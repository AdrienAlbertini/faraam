package com.adrienalbertini.faraam.app.common

class Constants {
    companion object {
        val API_BASE_PATH = "https://jsonplaceholder.typicode.com"
    }
}