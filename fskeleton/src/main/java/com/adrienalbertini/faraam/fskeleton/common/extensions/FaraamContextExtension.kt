package com.adrienalbertini.faraam.fskeleton.common.extensions

import android.content.Context
import android.net.ConnectivityManager

fun Context.isNetworkStatusAvailable(): Boolean {
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
    return connectivityManager?.activeNetworkInfo?.isConnected ?: false
}