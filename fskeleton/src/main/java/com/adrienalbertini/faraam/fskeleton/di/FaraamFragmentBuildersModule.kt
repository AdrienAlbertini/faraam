package com.adrienalbertini.faraam.fskeleton.di

import com.adrienalbertini.faraam.fskeleton.ui.FaraamBaseFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FaraamFragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeFaraamBaseFragment(): FaraamBaseFragment
}