package com.adrienalbertini.faraam.fskeleton.di

import com.adrienalbertini.faraam.fskeleton.ui.FaraamBaseActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FaraamActivityModule {
    @ContributesAndroidInjector(modules = [FaraamFragmentBuildersModule::class])
    abstract fun baseActivity(): FaraamBaseActivity
}