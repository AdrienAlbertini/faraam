package com.adrienalbertini.faraam.fskeleton.ui

import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.adrienalbertini.faraam.fskeleton.common.Log
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Base activity for Sounds.<br></br>
 * <br></br>
 * Not forget to call [initActionBar(toolbar, title)][.initActionBar] if you have an action bar.
 */
abstract class FaraamBaseActivity : AppCompatActivity(), HasSupportFragmentInjector {

    companion object {

        private val TAG = Log.tag(FaraamBaseActivity::class.java)

        const val EXTRA_FROM = "extra_from"
        const val EXTRA_VIA = "extra_via"

        /** Global boolean used to detect first activity shown for the app  */
        protected var firstScreen = true
    }

    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        // And store it
        setIntent(intent)
    }

    // This snippet hides the system bars.
    protected fun hideSystemUI() {
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                )
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector
}