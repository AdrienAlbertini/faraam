package com.adrienalbertini.faraam.fskeleton.common.inputfilters

import android.text.InputFilter
import android.text.Spanned

class AlphaNumericInputFilter: InputFilter {
    override fun filter(source: CharSequence?, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int): CharSequence? {
        // Only keep characters that are alphanumeric
        source ?: return null

        val builder = StringBuilder()
        for (i in start until end) {
            val c = source[i]
            if (Character.isLetterOrDigit(c)) {
                builder.append(c)
            }
        }

        // If all characters are valid, return null, otherwise only return the filtered characters
        val allCharactersValid = builder.length == end - start
        return if (allCharactersValid) null else builder.toString()
    }
}