package com.adrienalbertini.faraam.fskeleton.ui

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class ViewHolder<in T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    protected var context: Context = itemView.context

    abstract fun show(t: T)
}