package com.adrienalbertini.faraam.fskeleton.common.exceptions

import android.content.Context

abstract class FaraamNetworkEceptions(val context: Context, errMsg: String): RuntimeException(errMsg) {
    class NoServerConnectivityError(context: Context): FaraamNetworkEceptions(context, "No server connectivity error")
    class NoNetworkConnectivityError(context: Context): FaraamNetworkEceptions(context, "No network connectivity error")
    class UnexpectedError(context: Context): FaraamNetworkEceptions(context, "Unexpected error")
}