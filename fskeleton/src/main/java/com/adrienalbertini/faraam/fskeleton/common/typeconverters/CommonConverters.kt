package com.adrienalbertini.faraam.fskeleton.common.typeconverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class CommonConverters {
    companion object {
        val stringListType = object : TypeToken<MutableList<String>>() {}.type

        @JvmStatic
        @TypeConverter
        fun stringToList(jsonStr: String): MutableList<String>? {
            return Gson().fromJson(jsonStr, stringListType)
        }

        @JvmStatic
        @TypeConverter
        fun listToString(objs: MutableList<String>?): String {
            return Gson().toJson(objs, stringListType)
        }
    }
}