package com.adrienalbertini.faraam.fskeleton.common.extensions

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.adrienalbertini.faraam.fskeleton.di.FaraamViewModelFactory

inline fun <reified T: ViewModel> FragmentActivity.getViewModel(viewModelFactory: FaraamViewModelFactory): T {
    return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
}