package com.adrienalbertini.faraam.fskeleton.common

/**
 * Created by adrienalbertini on 3/7/18.
 */
interface IValidity {
    fun isValid(): Boolean
}