package com.adrienalbertini.faraam.fskeleton.common

interface IVia {
    val id: String get
}