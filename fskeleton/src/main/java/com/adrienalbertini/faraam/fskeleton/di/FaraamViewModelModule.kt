package com.adrienalbertini.faraam.fskeleton.di

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module


@Suppress("UNUSED")
@Module
abstract class FaraamViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: FaraamViewModelFactory): ViewModelProvider.Factory
}