package com.adrienalbertini.faraam.fskeleton.common.extensions

import android.net.Uri
import com.adrienalbertini.faraam.fskeleton.common.utilities.FaraamDateUtils
import com.google.gson.internal.bind.util.ISO8601Utils
import java.text.ParsePosition
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by adrienalbertini on 5/31/17.
 */

fun String.toDate(): Date? {
    var dt: Date? = null
    val supportedDateFormats: Array<String> = arrayOf("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ", "yyyy-MM-dd'T'HH:mm:ssZZZZZ")
    for (sdf in supportedDateFormats) {
        try {
            val date = SimpleDateFormat(sdf, Locale.getDefault()).parse(this)
            dt = FaraamDateUtils.getDateCurrentTimeZone(date.time)
            break
        } catch (pe: Throwable) { }
    }
    return dt
}

fun String.toISO8601(): Date {
    return ISO8601Utils.parse(this, ParsePosition(0))
}

fun String.toUri(): Uri = Uri.parse(this)

fun String?.valueIfNullOrEmpty(value: String): String {
    return if (this.isNullOrEmpty()) value  else this
}

fun String?.isValid(): Boolean {
    return !(this.isNullOrEmpty())
}

fun String?.validOrNull(): String? {
    return if (this.isNullOrEmpty()) null else this
}

fun String.replaceAll(oldValues: Array<String>, newValue: String, ignoreCase: Boolean = false): String {
    var finalStr = this
    oldValues.map { finalStr = finalStr.replace(it, newValue) }
    return finalStr
}

