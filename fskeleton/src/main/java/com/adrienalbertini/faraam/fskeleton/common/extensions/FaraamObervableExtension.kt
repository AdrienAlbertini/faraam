package com.adrienalbertini.faraam.fskeleton.common.extensions

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import io.reactivex.disposables.Disposable

private class LifecycleOnStopDisposable(obj: Disposable) : DefaultLifecycleObserver, Disposable by obj {
    override fun onStop(owner: LifecycleOwner) {
        dispose()
    }
}

private class LifecycleOnPauseDisposable(obj: Disposable) : DefaultLifecycleObserver, Disposable by obj {
    override fun onPause(owner: LifecycleOwner) {
        dispose()
    }
}

fun Disposable.attachToStopLifecycle(owner: LifecycleOwner): Disposable {
    owner.lifecycle.addObserver(LifecycleOnStopDisposable(this))
    return this
}

fun Disposable.attachToPauseLifecycle(owner: LifecycleOwner): Disposable {
    owner.lifecycle.addObserver(LifecycleOnPauseDisposable(this))
    return this
}