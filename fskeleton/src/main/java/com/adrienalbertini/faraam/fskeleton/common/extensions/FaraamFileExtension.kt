package com.adrienalbertini.faraam.fskeleton.common.extensions

import java.io.*


@Throws(Exception::class)
fun InputStream.streamToString(): String {
    val reader = BufferedReader(InputStreamReader(this))
    val sb = StringBuilder()
    var line: String? = reader.readLine()

    while (line != null) {
        sb.append(line).append("\n")
        line = reader.readLine()
    }
    reader.close()
    return sb.toString()
}

fun File.contentToString(): String? {
    val fileInputStream = FileInputStream(this)
    val ret = fileInputStream.streamToString()
    //Make sure you close all streams.
    fileInputStream.close()
    return ret
}

fun InputStream.toFile(file: File) {
    use { input ->
        file.outputStream().use { input.copyTo(it) }
    }
}