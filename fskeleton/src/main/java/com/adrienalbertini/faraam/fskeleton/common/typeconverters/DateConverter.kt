package com.adrienalbertini.faraam.fskeleton.common.typeconverters

import androidx.room.TypeConverter
import java.util.*

class DateConverter {
    companion object {
        @JvmStatic
        @TypeConverter
        fun toDate(timestamp: Long?) = timestamp?.let { Date(it) }

        @JvmStatic
        @TypeConverter
        fun toLong(date: Date?) = date?.time
    }
}