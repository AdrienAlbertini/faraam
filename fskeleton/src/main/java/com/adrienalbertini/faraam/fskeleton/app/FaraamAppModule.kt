package com.adrienalbertini.faraam.fskeleton.app

import com.adrienalbertini.faraam.fskeleton.di.FaraamViewModelModule
import dagger.Module

@Module(includes = [FaraamViewModelModule::class])
open class FaraamAppModule