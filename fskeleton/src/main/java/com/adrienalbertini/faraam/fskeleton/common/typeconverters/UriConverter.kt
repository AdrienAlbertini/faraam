package com.adrienalbertini.faraam.fskeleton.common.typeconverters

import android.net.Uri
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class UriConverter {
    companion object {
        private val objListType = object : TypeToken<List<Uri>>() {}.type!!

        @JvmStatic
        @TypeConverter
        fun stringToUri(str: String?): Uri {
            return Uri.parse(str ?: "")
        }

        @JvmStatic
        @TypeConverter
        fun mimeTypeToString(obj: Uri?): String {
            return obj?.toString() ?: ""
        }

        @JvmStatic
        @TypeConverter
        fun stringToMimeTypeList(jsonStr: String): List<Uri> {
            return Gson().fromJson(jsonStr, objListType)
        }

        @JvmStatic
        @TypeConverter
        fun mimeTypeListToString(objs: List<Uri>): String {
            return Gson().toJson(objs, objListType)
        }
    }
}