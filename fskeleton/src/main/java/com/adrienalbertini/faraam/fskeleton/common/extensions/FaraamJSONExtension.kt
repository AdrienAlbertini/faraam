package com.adrienalbertini.faraam.fskeleton.common.extensions

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by adrienalbertini on 5/17/17.
 */

@Throws(JSONException::class)
fun jsonToMap(json: JSONObject?): Map<String, Any> {
    val map = HashMap<String, Any>()

    if (json == null) return map
    val keysItr = json.keys()
    while (keysItr.hasNext()) {
        val key = keysItr.next()
        var value = json.get(key.toString())

        if (value is JSONArray) {
            value = jsonToList(value)
        } else if (value is JSONObject) {
            value = jsonToMap(value)
        }
        map.put(key.toString(), value)
    }
    return map
}

@Throws(JSONException::class)
fun jsonToList(json: JSONArray?): List<Any> {
    val list = ArrayList<Any>()
    if (json == null) return list
    for (i in 0 until json.length()) {
        var value = json.get(i)
        if (value is JSONArray) {
            value = jsonToList(value)
        } else if (value is JSONObject) {
            value = jsonToMap(value)
        }
        list.add(value)
    }
    return list
}