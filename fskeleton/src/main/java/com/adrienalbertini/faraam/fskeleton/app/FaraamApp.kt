package com.adrienalbertini.faraam.fskeleton.app

import android.app.Activity
import android.app.Application
import com.adrienalbertini.faraam.fskeleton.common.Log
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

abstract class FaraamApp: Application(), HasActivityInjector {
    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        Log.addLogger(Log.ANDROID_LOGGER)
    }

    abstract fun injectApp(app: FaraamApp)

    override fun activityInjector() = dispatchingAndroidInjector
}