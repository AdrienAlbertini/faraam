package com.adrienalbertini.faraam.fskeleton.common

enum class FaraamScreen(val id: String) {
    NONE("")
    ;

    companion object {
        fun fromId(id: String?): FaraamScreen {
            return values().firstOrNull { it.id == id } ?: NONE
        }
    }
}