package com.adrienalbertini.faraam.fskeleton.di


import javax.inject.Scope

/**
 * Scope for dependencies with a limited lifecycle related to UI (activity, service).
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class UILifecycleScope
