package com.adrienalbertini.faraam.fskeleton.app

import com.adrienalbertini.faraam.fskeleton.di.FaraamActivityModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton



@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    FaraamAppModule::class,
    FaraamActivityModule::class
])
interface FaraamAppComponent {}