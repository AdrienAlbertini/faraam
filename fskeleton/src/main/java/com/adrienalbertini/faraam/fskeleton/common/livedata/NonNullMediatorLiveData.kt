package com.adrienalbertini.faraam.fskeleton.common.livedata

import androidx.lifecycle.MediatorLiveData

class NonNullMediatorLiveData<T> : MediatorLiveData<T>()