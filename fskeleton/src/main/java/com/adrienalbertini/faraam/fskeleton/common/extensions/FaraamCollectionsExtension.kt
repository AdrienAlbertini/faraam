package com.adrienalbertini.faraam.fskeleton.common.extensions

/**
 * Created by adrienalbertini on 7/17/17.
 */

fun <T> Iterable<T>.firstNotNull(): T? {
    this.map { if (it != null) return it }
    return null
}

fun <T> List<T>.toArrayList(): ArrayList<T> {
    return ArrayList(this)
}