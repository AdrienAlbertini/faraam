package com.adrienalbertini.faraam.fskeleton.common.extensions

import android.os.Parcel

/**
 * Created by adrienalbertini on 11/9/17.
 */

fun Parcel.writeBoolean(value: Boolean) {
    this.writeByte((if (value) 1 else 0).toByte())
}

fun Parcel.readBoolean(): Boolean = this.readByte().toInt() != 0