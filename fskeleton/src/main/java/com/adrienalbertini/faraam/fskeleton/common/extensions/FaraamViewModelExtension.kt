package com.adrienalbertini.faraam.fskeleton.common.extensions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*

@Suppress("UNCHECKED_CAST")
inline fun <reified T : ViewModel> FragmentActivity.getViewModel(factory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, factory)[T::class.java]
}

@Suppress("UNCHECKED_CAST")
inline fun <reified T : ViewModel> Fragment.getViewModel(factory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, factory)[T::class.java]
}

inline fun <reified T : ViewModel> FragmentActivity.withViewModel(
        factory: ViewModelProvider.Factory,
        body: T.() -> Unit
): T {
    val vm = getViewModel<T>(factory)
    vm.body()
    return vm
}

inline fun <reified T : ViewModel> Fragment.withViewModel(
        factory: ViewModelProvider.Factory,
        body: T.() -> Unit
): T {
    val vm = getViewModel<T>(factory)
    vm.body()
    return vm
}

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) {
    liveData.observe(this, Observer(body))
}