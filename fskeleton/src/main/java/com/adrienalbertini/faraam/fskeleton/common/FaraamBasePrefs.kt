package com.adrienalbertini.faraam.fskeleton.common

import android.content.Context
import android.content.SharedPreferences
import java.util.*
import kotlin.collections.ArrayList

abstract class FaraamBasePrefs(protected val context: Context, prefsName: String) {
    companion object {
        @JvmField val allPrefs = ArrayList<String>()

        @JvmStatic fun clearAllPrefs(context: Context) {
            for (pref in allPrefs) {
                context.getSharedPreferences(pref, 0).edit().clear().apply()
            }
        }
    }

    protected val prefs: SharedPreferences

    init {
        allPrefs.add(prefsName)
        this.prefs = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)
    }

    protected operator fun set(key: String, value: String?) {
        prefs.edit().putString(key, value).apply()
    }

    protected fun getStringList(key: String, value: ArrayList<String>?): ArrayList<String>? {
        val set = prefs.getStringSet(key, if (value == null) null else HashSet(value))

        return if (set == null) null else ArrayList(set)
    }

    protected fun setStringList(key: String, value: List<String>?) {
        prefs.edit().putStringSet(key, if (value == null) null else HashSet(value)).apply()
    }
}
