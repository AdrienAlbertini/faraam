package com.adrienalbertini.faraam.fskeleton.common.utilities

import android.content.Context
import android.text.format.DateUtils
import com.adrienalbertini.faraam.fskeleton.R
import org.joda.time.Interval
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Gary on 19/05/16.
 */
object FaraamDateUtils {
    const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"

    fun today(): Date {
        return Calendar.getInstance().time
    }

    private fun inRange(value: Long, min: Long, max: Long): Boolean {
        return value in min..(max - 1)
    }

    fun getDateCurrentTimeZone(timestamp: Long): Date {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = timestamp
        return calendar.time
    }

    /**
     * Obtain Calendar from iso date string
     * @param iso8601string
     * *
     * @return
     * *
     * @throws ParseException
     */
    @Throws(ParseException::class)
    fun toCalendar(iso8601string: String): Calendar {
        val calendar = GregorianCalendar.getInstance()
        var s = iso8601string.replace("Z", "+00:00")
        try {
            s = s.substring(0, 22) + s.substring(23)
        } catch (e: IndexOutOfBoundsException) {
            throw ParseException("Invalid length", 0)
        }

        val date = SimpleDateFormat(DATE_FORMAT).parse(s)
        calendar.time = date
        return calendar
    }

    /**
     * Return elapse time between now and the given date
     * @param date
     * *
     * @param unit
     * *
     * @return elapse time in the given time unit
     */
    fun getElapseTimeFromNow(date: Date, unit: Long): Long {
        val elapse = System.currentTimeMillis() - date.time
        return elapse / unit
    }

    /**
     * Return a string interval between given date and current date
     * now, 2m, 4h, 1w...
     * @param date
     * *
     * @return
     */
    fun simpleElapseTime(date: Date?, context: Context? = null, now: Date? = null): String {
        if (date == null) {
            return ""
        }

        val timeInterval = (now?.time ?: System.currentTimeMillis()) - date.time

        // Display now 0 < 60 sec
        if (timeInterval <= 0 || inRange(timeInterval, 0, DateUtils.MINUTE_IN_MILLIS)) {
            return if (context != null) context.resources.getString(R.string.now) else "Now"
        }
        // Display {x} min 1 < 60 min
        else if (inRange(timeInterval, DateUtils.MINUTE_IN_MILLIS, DateUtils.HOUR_IN_MILLIS)) {
            return String.format("%d min", timeInterval / 1000 / 60)
        }
        // Display {x}h 1 < 24 hours
        else if (inRange(timeInterval, DateUtils.HOUR_IN_MILLIS, DateUtils.DAY_IN_MILLIS)) {
            return String.format("%d h", timeInterval / 1000 / (60 * 60).toLong())
        }
        // Display {x}d 1 < 7 days
        else if (inRange(timeInterval, DateUtils.DAY_IN_MILLIS, DateUtils.WEEK_IN_MILLIS)) {
            return String.format("%d d", timeInterval / 1000 / (60 * 60 * 24).toLong())
        }
        // Display {x}w 1 < 4 weeks
        else if (inRange(timeInterval, DateUtils.WEEK_IN_MILLIS, DateUtils.WEEK_IN_MILLIS * 4)) {
            return String.format("%d w", timeInterval / 1000 / (60 * 60 * 24 * 7).toLong())
        }
        // Display {x} 1 < 12 month
        else if (inRange(timeInterval, DateUtils.WEEK_IN_MILLIS * 4, DateUtils.YEAR_IN_MILLIS)) {
            val month = timeInterval / 1000 / (60 * 60 * 24).toLong() / 30
            return String.format("%d m", if (month > 0) month else 1)
        }
        // Display {x} y
        else {
            return String.format("%d y", timeInterval / 1000 / (60 * 60 * 24 * 365).toLong())
        }
    }

    fun shortDate(date: Date?): String? {
        if (date == null) {
            return null
        }
        try {
            val today = today()
            val interval = Interval(date.time, today.time).toPeriod()
            val formatter: SimpleDateFormat
            if (interval.years < 1) {
                if (interval.months < 1) {
                    if (interval.days < 1) {
                        formatter = SimpleDateFormat("HH:mm", Locale.getDefault())
                    }
                    else if (interval.days < 8) {
                        formatter = SimpleDateFormat("EEE", Locale.getDefault())
                    }
                    else {
                        formatter = SimpleDateFormat("MMM dd", Locale.getDefault())
                    }
                }
                else {
                    formatter = SimpleDateFormat("MMM dd", Locale.getDefault())
                }
            }
            else {
                formatter = SimpleDateFormat("MMM dd", Locale.getDefault())
            }
            return formatter.format(date)
        }
        catch (exception: Exception) {
            return null
        }
    }

    fun displayDateAndTime(date: Date?): String? {
        if (date == null) {
            return null
        }
        try {
            val today = today()
            val interval = Interval(date.time, today.time).toPeriod()
            val formatter: SimpleDateFormat
            if (interval.days < 1) {
                formatter = SimpleDateFormat("HH:mm", Locale.getDefault())
            }
            else if (interval.days < 8) {
                formatter = SimpleDateFormat  ("EEEE HH:mm", Locale.getDefault())
            }
            else {
                formatter = SimpleDateFormat("MMMM d", Locale.getDefault())
            }
            return formatter.format(date)
        }
        catch (exception: Exception) {
            return null
        }
    }

    fun displayNow(): String? {
        try {
            val df = DateFormat.getDateInstance(DateFormat.MEDIUM)
            return df.format(today())
        }
        catch (exception: Exception) {
            return null
        }
    }
}