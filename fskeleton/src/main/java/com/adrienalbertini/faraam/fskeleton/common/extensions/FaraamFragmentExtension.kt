package com.adrienalbertini.faraam.fskeleton.common.extensions

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.adrienalbertini.faraam.fskeleton.common.IVia
import com.adrienalbertini.faraam.fskeleton.common.FaraamScreen
import com.adrienalbertini.faraam.fskeleton.ui.FaraamBaseActivity.Companion.EXTRA_FROM
import com.adrienalbertini.faraam.fskeleton.ui.FaraamBaseActivity.Companion.EXTRA_VIA

/**
 * Created by Gary on 08/02/18.
 */

fun Fragment.saveArguments(action: Bundle.() -> Unit) {
    val bundle = this.arguments ?: Bundle()
    action(bundle)
    this.arguments = bundle
}

fun Fragment.saveFromAndVia(from: FaraamScreen, via: IVia) {
    val bundle = this.arguments ?: Bundle()
    bundle.putString(EXTRA_FROM, from.id)
    bundle.putString(EXTRA_VIA, via.id)
    this.arguments = bundle
}

fun Fragment.getFrom(): FaraamScreen {
    return this.arguments?.getString(EXTRA_FROM)?.let { FaraamScreen.fromId(it) } ?: FaraamScreen.NONE
}

fun Fragment.getVia(): String {
    return this.arguments?.getString(EXTRA_VIA) ?: ""
}