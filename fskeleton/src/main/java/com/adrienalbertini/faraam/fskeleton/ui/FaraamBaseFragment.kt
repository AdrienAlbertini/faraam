package com.adrienalbertini.faraam.fskeleton.ui

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.adrienalbertini.faraam.fskeleton.di.FaraamInjectable

abstract class FaraamBaseFragment: Fragment(), FaraamInjectable {
    /**
     * Created to be able to override in tests
     */
    fun navController() = NavHostFragment.findNavController(this)
}
