package com.adrienalbertini.faraam.fskeleton.common.extensions

import android.view.View

/**
 * Created by adrienalbertini on 5/17/17.
 */

fun Boolean?.isTrue(): Boolean {
    return this != null && this == true
}

fun Boolean?.isFalse(): Boolean {
    return this != null && this == false
}

fun Boolean.visibility(): Int {
    return if (this) View.VISIBLE else View.GONE
}