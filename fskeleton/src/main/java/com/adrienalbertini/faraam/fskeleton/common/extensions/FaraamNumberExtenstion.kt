package com.adrienalbertini.faraam.fskeleton.common.extensions

import java.math.BigDecimal

/**
 * Created by Gary on 07/08/17.
 */

// Math
fun Float.round(decimalPlace: Int = 2): Float {
    var bd = BigDecimal(java.lang.Float.toString(this))
    bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP)
    return bd.toFloat()
}