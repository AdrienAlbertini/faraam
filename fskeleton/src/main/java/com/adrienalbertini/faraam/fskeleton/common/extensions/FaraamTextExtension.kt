package com.adrienalbertini.faraam.fskeleton.common.extensions

/**
 * Created by adrienalbertini on 5/15/17.
 */

fun CharSequence?.isValid(): Boolean {
    if (this == null || this.isEmpty()) {
        return false
    }
    return true
}