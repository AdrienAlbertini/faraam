package com.adrienalbertini.faraam.fskeleton.common


import java.util.concurrent.CopyOnWriteArrayList

/**
 * Simple logger inspired by
 * [JakeWharton/timber](https://github.com/JakeWharton/timber).
 */
object Log {
    private val TAG = tag(Log::class.java)

    /**
     * Log with [System.out] stream.<br></br>
     * Default logger used when no one are available (in unit tests for example).
     */
    var JAVA_LOGGER: Logger = object : Logger {
        override fun log(priority: Int, tag: String, msg: String) {
            val level: String = when (priority) {
                android.util.Log.DEBUG -> "D"
                android.util.Log.INFO -> "I"
                android.util.Log.WARN -> "W"
                android.util.Log.ERROR -> "E"
                else -> "U"
            }
            println(String.format("%s/ %s: %s", level, tag, msg))
        }

        override fun e(tag: String, msg: String, tr: Throwable) {
            log(android.util.Log.ERROR, tag, msg)
            tr.printStackTrace(System.out)
        }

        override operator fun set(key: String, value: String) {
            println("$TAG: Set($key, \"$value\")")
        }
    }

    /** Log with [android.util.Log] class.  */
    var ANDROID_LOGGER: Logger = object : Logger {
        override fun log(priority: Int, tag: String, msg: String) {
            android.util.Log.println(priority, tag, msg)
        }

        override fun e(tag: String, msg: String, tr: Throwable) {
            android.util.Log.e(tag, msg, tr)
        }

        override operator fun set(key: String, value: String) {
            android.util.Log.i(TAG, "Set($key, \"$value\")")
        }
    }

    // Implementation

    private val LOGGERS = CopyOnWriteArrayList<Logger>()

    fun tag(c: Class<*>): String {
        return "soundsapp." + c.simpleName
    }

    fun v(tag: String, msg: String) {
        println(android.util.Log.VERBOSE, tag, null, msg)
    }

    fun d(tag: String, msg: String) {
        println(android.util.Log.DEBUG, tag, null, msg)
    }

    fun i(tag: String, msg: String) {
        println(android.util.Log.INFO, tag, null, msg)
    }

    fun w(tag: String, msg: String) {
        println(android.util.Log.WARN, tag, null, msg)
    }

    fun e(tag: String, msg: String) {
        println(android.util.Log.ERROR, tag, null, msg)
    }

    fun e(tag: String, msg: String, th: Throwable) {
        println(android.util.Log.ERROR, tag, th, msg)
    }

    fun e(tag: String, th: Throwable) {
        println(android.util.Log.ERROR, tag, th, th.toString())
    }

    operator fun set(key: String, value: String) {
        setString(key, value)
    }

    interface Logger {
        fun log(priority: Int, tag: String, msg: String)
        fun e(tag: String, msg: String, tr: Throwable)
        operator fun set(key: String, value: String)
    }

    fun addLogger(logger: Logger) {
        if (!LOGGERS.contains(logger)) {
            LOGGERS.add(logger)
        }
    }

    private fun println(priority: Int, tag: String, tr: Throwable?, msg: String) {
        if (LOGGERS.size == 0) {
            // Use default logger
            if (tr == null) {
                JAVA_LOGGER.log(priority, tag, msg)
            } else {
                JAVA_LOGGER.e(tag, msg, tr)
            }
        } else {
            for (logger in LOGGERS) {
                if (tr == null) {
                    logger.log(priority, tag, msg)
                } else {
                    logger.e(tag, msg, tr)
                }
            }
        }
    }

    private fun setString(key: String, value: String) {
        if (LOGGERS.size == 0) {
            // Use default logger
            JAVA_LOGGER[key] = value
        } else {
            for (logger in LOGGERS) {
                logger[key] = value
            }
        }
    }
}
