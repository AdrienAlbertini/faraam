package com.adrienalbertini.faraam.fskeleton.di

import android.app.Service
import android.content.BroadcastReceiver
import com.adrienalbertini.faraam.fskeleton.app.FaraamAppComponent
import dagger.Component

/**
 * Unless [FaraamActivityComponent], this component is the default component
 * for non Activity UI instances (like Android [Service] or [BroadcastReceiver]).<br></br>
 * <br></br>
 * It allows these UI instances to use [UILifecycleScope] dependencies,
 * shared between fragments but also by UI instances dependencies.
 */
@UILifecycleScope
@Component(dependencies = [FaraamAppComponent::class])
interface UILifecycleComponent
