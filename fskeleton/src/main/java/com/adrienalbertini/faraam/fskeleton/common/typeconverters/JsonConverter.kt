package com.adrienalbertini.faraam.fskeleton.common.typeconverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken

class JsonConverter {
    companion object {
        private val objType = object : TypeToken<JsonObject>() {}.type!!
        private val objListType = object : TypeToken<MutableList<JsonObject>>() {}.type!!

        @JvmStatic
        @TypeConverter
        fun stringToAny(jsonStr: String): JsonObject {
            return Gson().fromJson(jsonStr, objType)
        }

        @JvmStatic
        @TypeConverter
        fun anyToString(any: JsonObject): String {
            return Gson().toJson(any)
        }

        @JvmStatic
        @TypeConverter
        fun stringToAnyList(jsonStr: String): MutableList<JsonObject> {
            return Gson().fromJson(jsonStr, objListType)
        }

        @JvmStatic
        @TypeConverter
        fun anyListToString(list: MutableList<JsonObject>): String {
            return Gson().toJson(list)
        }
    }
}