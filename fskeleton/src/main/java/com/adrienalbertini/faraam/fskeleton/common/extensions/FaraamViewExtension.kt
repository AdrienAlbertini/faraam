package com.adrienalbertini.faraam.fskeleton.common.extensions

import android.view.View
import androidx.constraintlayout.widget.Group

fun Group.addOnClickListener(listener: View.OnClickListener) {
    referencedIds.map { id ->
        rootView.findViewById<View>(id).setOnClickListener(listener)
    }
}
